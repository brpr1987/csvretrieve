# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Problem Statemenet: 
You are developing an application (preferably in Java/C++/C#) that will allow multiple clients to access student information in a database.  The “database” will be in a FILE.  Data in the file shall be in the following CSV format:
 
Class, Professor, Student ID
 
Class = History, Chemistry, Physics, Mathematics [ONLY 4 types]
Professor = “a string”
Student ID = “an integer”
 
The application shall be used to retrieve the following information:
·         How many students are registered for a Class?
·         How many students take more than one Class?
 
 
Additional Requirements:
·         UI is not required
·         Simulate multiple clients accessing the application
·         The "file" can be updated with new information once in a while. (You are not required to provide functionality to edit the file.)
·         Bonus: Include unit tests that validate the code you have written
 
Sample Data:
Chemistry, Joseph, 1234
Chemistry, Jane, 3455
History, Jane, 3455
Mathematics, Doe, 56767

### How do I get set up? ###
If you have maven installed, then you could run
 $mvn install 
and then invoke the class file from your terminal to run the test case or the class.
The test file is in 
 $<project home>/test/java/com/csv/csvRetrieve/StudentRetrieveTest.java
Please use the corresponding target file from the target folder instead.
Changes to the pom file build config can change it to a jar / war later.

### Contribution guidelines ###
later 

### Who do I talk to? ###

br_prashant@hotmail.com