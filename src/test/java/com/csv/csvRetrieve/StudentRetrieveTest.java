package com.csv.csvRetrieve;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;

/**
 *
 *
 * Created by brprashant on 3/7/16.
 */
public class StudentRetrieveTest {

    private static StudentRetrieve studentRetrieve = StudentRetrieve.getInstance();

    @Before
    public void beforeEverything(){
        studentRetrieve.reset();
    }

    @Test
    public void testStudentRetrieval() throws IOException {
        URL url = this.getClass().getClassLoader().getResource("TestFile.csv");
        assert url != null;
        assert StringUtils.isEmpty(studentRetrieve.getFullFilePath());
        studentRetrieve.setFullFilePath(url.getPath());
        studentRetrieve.loadFile();
        Assert.assertTrue(studentRetrieve.totalStudentsCount()==9);
        Assert.assertTrue(studentRetrieve.getMultipleClassStudentCount()==5);
        Assert.assertTrue(studentRetrieve.getStudentsCount(Class.PHYSICS)==5);
        Assert.assertTrue(studentRetrieve.getStudentsCount(Class.MATHEMATICS)==7);
        Assert.assertTrue(studentRetrieve.getStudentsCount(Class.CHEMISTRY)==3);
        Assert.assertTrue(studentRetrieve.getStudentsCount(Class.HISTORY)==3);
    }

    @Test(expected = IllegalStateException.class)
    public void testLoadFileForState() throws IOException{
        studentRetrieve.loadFile();
    }

    @Test(expected = IOException.class)
    public void testLoadFileForIO() throws IOException{
        studentRetrieve.setFullFilePath("something that does not exist as a file name from the class loader.");
        studentRetrieve.loadFile();
    }
}
