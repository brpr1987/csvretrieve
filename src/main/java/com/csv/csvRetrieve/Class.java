package com.csv.csvRetrieve;

/**
 *
 *
 * Created by brprashant on 3/7/16.
 */
public enum Class {

    PHYSICS("Physics"),
    CHEMISTRY("Chemistry"),
    MATHEMATICS("Mathematics"),
    HISTORY("History");

    private String subject;

    private  String getSubject() {
        return subject;
    }

    private void setSubject(String subject) {
        this.subject = subject;
    }

    Class(String subject){
        this.subject = subject;
    }

    static Class valueOfString(String text){
        switch(text.toLowerCase()){
            case "physics" : return PHYSICS;
            case "chemistry" : return CHEMISTRY;
            case "mathematics" : return MATHEMATICS;
            case "history" : return HISTORY;
            default : return null;
        }
    }

}
