package com.csv.csvRetrieve;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.opencsv.CSVReader;
import org.apache.commons.lang3.StringUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;
import java.util.concurrent.Semaphore;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Description goes here.
 * <p>
 * Created by brprashant on 3/7/16.
 */
public class StudentRetrieve {

    private static final Logger logger = LoggerFactory.getLogger(StudentRetrieve.class);

    private String fullFilePath;

    private ArrayList<Integer> studentRow = new ArrayList<>();

    private Map<Integer, String> studentRowProfessor = new HashMap<>();

    private Map<Integer, Class> studentRowClass = new HashMap<>();

    private Set<Integer> students = new HashSet<>();

    private static final int WRITE_SEMAPHORES = 1;

    private static final int READ_SEMAPHORES = 10;

    private static final Semaphore writeSemaphore = new Semaphore(WRITE_SEMAPHORES);

    private static final Semaphore readSemaphore = new Semaphore(READ_SEMAPHORES, true);

    /**
     * private Constructor to limit instances in a system.
     * use getInstance() for an instance of this class.
     * Made so since the term database was used in the problem statement.
     */
    private StudentRetrieve() {
    }

    /**
     * Creating empty instance on initialization.
     * Will be returned using getInstance().
     */
    private static StudentRetrieve studentRetrieve = new StudentRetrieve();

    /**
     * Will provide an existing instance of StudentRetrieve class.
     * You will need to set the file path and load before you utilize other methods.
     *
     * @return studentRetrieve an instance of StudentRetrieve
     */
    public static StudentRetrieve getInstance() {
        return studentRetrieve;
    }

    /**
     * @return returns the current full file Path .
     */
    public String getFullFilePath() {
        return fullFilePath;
    }

    /**
     * @param fullFilePath the file including its path used as the data base resource.
     */
    public void setFullFilePath(String fullFilePath) {
        this.fullFilePath = fullFilePath;
    }

    /**
     * Attempts to load the file available.
     * Throws Illegal state exception if fullFilePath is not set.
     * Blocks all reads and write semaphores before attempting the load.
     * Processes the file line by line.
     * Runs an expensive algorithms for not processing duplicates at the moment.
     * Caches the result for further data processing.
     *
     * @throws IOException when there is an issue with the file name provided
     */
    public void loadFile() throws IOException {
        Preconditions.checkState(fullFilePath != null, "An expected full file path is not available.");
        CSVReader reader = null;

        try {
            readSemaphore.acquire(READ_SEMAPHORES);
            writeSemaphore.acquire(WRITE_SEMAPHORES);
        } catch (InterruptedException ie) {
            logger.error("Oops semaphore error", "Error acquiring semaphores for loading file", ie);
        }
        try {
            reader = new CSVReader(new FileReader(fullFilePath));
            String[] row;
            while ((row = reader.readNext()) != null) {
                processLine(row);
            }
        } finally {
            readSemaphore.release(READ_SEMAPHORES);
            writeSemaphore.release(WRITE_SEMAPHORES);
            if (null != reader) {
                try {
                    reader.close();
                } catch (IOException e) {
                    logger.info("Error closing opened reader for file :{}.", fullFilePath, e);
                }
            }
        }
    }

    private boolean processLine(String[] row) {
        Preconditions.checkState(row.length == 3, "The row read from the CSV file does not have expected number of columns. It has {} columns", row.length);
        Preconditions.checkNotNull(row[0], "A column did not have a value to proceed.");
        Preconditions.checkNotNull(row[1], "A column did not have a value to proceed.");
        Preconditions.checkNotNull(row[2], "A column did not have a value to proceed.");
        if (StringUtils.equalsIgnoreCase(row[0], "Class")
                && StringUtils.equalsIgnoreCase(row[1], "Professor")
                && StringUtils.equalsIgnoreCase(row[2].replaceAll(" ", ""), "StudentId")) {
            return true;
        }

        Class subject = Class.valueOfString(row[0]);
        Preconditions.checkNotNull(subject, "The value : {}, could not be mapped to a correct subject value.", row[0]);

        String professor = Strings.nullToEmpty(row[1]);
        if (StringUtils.isBlank(professor)) {
            logger.info("An empty professor value was processed from the file.");
        }

        int studentId = 0;
        try {
            studentId = Integer.parseInt(row[2]);
        } catch (NumberFormatException nfe) {
            logger.error("The value : {}, could not be translated into a valid integer to be used as studentID.", row[2], nfe);
        }
        Preconditions.checkState(studentId > 0, "StudentID, an ID, should be positive.");

        // This check on duplicate rows is making it a n^2 algorithm!
        // Not cool mate, Not cool!
        if (!isDuplicate(studentId, professor, subject)) {
            studentRow.add(studentId);
            int index = studentRow.size() - 1;
            studentRowClass.put(index, subject);
            studentRowProfessor.put(index, professor);
            students.add(studentId);
        } else {
            logger.debug("A duplicate row was identified for student:{}, in class:{}, with professor:{}.", studentId, subject, professor);
        }
        return true;
    }

    private boolean isDuplicate(int studentId, String professor, Class subject) {
        Preconditions.checkNotNull(studentId, "studentId cannot be null or empty.");
        if (!students.contains(studentId)) {
            return false;
        }
        Preconditions.checkNotNull(subject, "Class value can not be null or empty.");
        Preconditions.checkNotNull(professor, "Professor cannot be null.");

        int rows = studentRow.size();
        for (int i = 0; i < rows; i++) {
            int currStudent = studentRow.get(i);
            if (currStudent == studentId) {
                if (studentRowClass.get(i).equals(subject)) {
                    if (studentRowProfessor.get(i).equalsIgnoreCase(professor)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public int getStudentsCount(Class subject) {
        try {
            readSemaphore.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        int classCount = 0;
        try {
            int rows = studentRow.size();
            for (int i = 0; i < rows; i++) {
                Class currentClass = studentRowClass.get(i);
                if (subject.equals(currentClass)) {
                    classCount = classCount + 1;
                }
            }
        } finally {
            readSemaphore.release();
        }
        return classCount;
    }

    public int getMultipleClassStudentCount() {
        try {
            readSemaphore.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        int studentCount = 0;
        try {
            Map<Integer, Integer> studentClassesMap = new HashMap<>();
            for (Integer aStudentRow : studentRow) {
                if (studentClassesMap.containsKey(aStudentRow)) {
                    int currentStudentClassCount = studentClassesMap.get(aStudentRow);
                    if (currentStudentClassCount == 1) {
                        studentCount = studentCount + 1;
                    }
                    studentClassesMap.put(aStudentRow, currentStudentClassCount + 1);
                } else {
                    studentClassesMap.put(aStudentRow, 1);
                }
            }
        } finally {
            readSemaphore.release();
        }
        return studentCount;
    }

    public int totalStudentsCount() {
        return students.size();
    }

    protected void reset() {
        try {
            readSemaphore.acquire(READ_SEMAPHORES);
            writeSemaphore.acquire(WRITE_SEMAPHORES);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        fullFilePath = null;
        studentRow = new ArrayList<>();
        studentRowProfessor = new HashMap<>();
        studentRowClass = new HashMap<>();
        students = new HashSet<>();

        readSemaphore.release(READ_SEMAPHORES);
        writeSemaphore.release(WRITE_SEMAPHORES);
    }

//    public void currentStats() {
//        System.out.println("The unique count of students processed : " + totalStudentsCount());
//        System.out.println("Size of StudentRow " + studentRow.size());
//        System.out.println("Size of StudentRow professor " + studentRowProfessor.size());
//        System.out.println("Size of StudentRow Class " + studentRowClass.size());
//        System.out.println("Number of students in multiple classes " + getMultipleClassStudentCount());
//        System.out.println("Number of students in Physics " + getStudentsCount(Class.PHYSICS));
//        System.out.println("Number of students in Mathematics " + getStudentsCount(Class.MATHEMATICS));
//        System.out.println("Number of students in Chemistry " + getStudentsCount(Class.CHEMISTRY));
//        System.out.println("Number of students in History " + getStudentsCount(Class.HISTORY));
//    }

}
